## mal-yarn - test project for GitLab Package Hunter

The [43. #EveryoneCanContribute cafe](https://everyonecancontribute.com/post/2021-08-18-cafe-43-more-package-dependency-hunting-with-gitlab/) shows this project in a live demo with Package Hunter. 

### Requirements

- [Package Hunter](https://gitlab.com/gitlab-org/security-products/package-hunter) running as server, accepting connections
- [Package Hunter CLI](https://gitlab.com/gitlab-org/security-products/package-hunter) or curl to upload the project to the server

### Tests

[package.json](package.json) provides a prepared command which does outbound connections, other than NPM related.

Goal is to upload to Package Hunter and receive the security report.

```shell
$ tar czf mal-yarn.tgz . --transform 's,^,mal-yarn/,'

$ curl -vvvvv -H 'Content-Type: application/octet-stream' --data-binary @mal-yarn.tgz http://localhost:3000/monitor/project/yarn
```
